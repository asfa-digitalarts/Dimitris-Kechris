
let road;
let outoforder;
let img; 

function preload() {

  img = createImg('data/choose.jpg');
  road = createVideo('data/road.mp4');
  road.position(300, 130);
  road.size(640, 360);
  outoforder =createVideo('data/outoforder.mp4');
  outoforder.position(windowWidth/4, windowHeight/4);  
  outoforder.size(640, 360);
  img.position(windowWidth/4, windowHeight/4); 
}

function setup() {
  createCanvas (windowWidth, windowHeight);
  background(0);
  outoforder.hide();
  road.hide();
}

function draw() {
  background (0);

}
function keyPressed() {

  if (keyCode===ENTER) {
    createCanvas (windowWidth, windowHeight);
    img.hide();
  background (0);

    outoforder.pause();
    outoforder.hide();
    road.show();
    road.play();
    road.loop();
  } 
  if (keyCode===BACKSPACE) {
    img.hide();
  background (0);

    road.pause();
    road.hide();
    outoforder.show();
    outoforder.play();
    outoforder.loop();
  }
}

function mousePressed() {

  let fs = fullscreen();
  fullscreen(!fs);
  background(0);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  background(0);
}
