let song;
let song2;
let song3;
let img; 
let img2;
let img3;
let november;

function preload() {

  november = createVideo('data/november.mp4');
  song = loadSound('data/violence.wav');
  song2 = loadSound('data/injury.wav');
  song3 = loadSound('data/clap.wav');
}

function setup() {

  createCanvas(windowWidth, windowHeight);
  background (0, 0, 0); 
  november.hide();
  november.pause();
  img = createImg('data/aura.jpg');
  img2 = createImg('data/many.jpg');
  img3 = createImg('data/roof.jpg');
  img.hide();
  img2.hide();
  img3.hide();
  song.pause();
  song2.pause();
  song3.pause();
}

function draw() {

  background (0, 0, 0);
}

function keyPressed() {



  if (key==="ArrowUp") {  

    img2.hide();
    img3.hide();

    img.show();
    img.position(0, 0, 900, 600); 
    song2.stop();
    song3.stop();
    song.play();
    song.loop();
    november.hide();
    november.pause();
  }

  if (key==="ArrowDown") { 
    img2.show();
    img2.position (150, 0);
    img3.hide();
    img.hide();
    song.stop();
    song3.stop();
    song2.play();
    song2.loop();
    november.hide();
    november.pause();
  }
  if (key==="ArrowLeft") {
    img2.hide();
    img.hide();
    img3.show();
    img3.position (windowWidth/2, windowHeight/2, 300, 300);
    song.stop();
    song2.stop();
    song3.play();
    song3.loop();
    november.hide();
    november.pause();
  }

  if (key==="ArrowRight") {


    song3.stop();
    img2.hide();
    img.hide();
    november.hide();
    november.pause();
  }


  if (key==="Enter") { 
    img.hide();
    img2.hide();
    img3.hide();
    song.stop();
    song2.stop();
    song3.stop();
    november.show();
    november.position(0, 0);
    november.size(640, 360);
    november.play();
    november.loop();
  }
}


function mousePressed() {
  let fs = fullscreen();
  fullscreen(!fs);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
